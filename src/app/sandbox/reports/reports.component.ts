import { SearchPopupComponent } from './../search-popup/search-popup.component';
import { StatisticsPopupComponent } from './../statistics-popup/statistics-popup.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddNewRowPopupComponent } from '../add-new-row-popup/add-new-row-popup.component';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  displayedColumns = ['Indicator', 'RulesofIndicator', 'Sector', 'Unit', 'Frequency', 'Nov', 'Dec', 'Actions', 'dots'];
  dataSource = ELEMENT_DATA
  showDropdown: boolean
  indicatorDropdown: boolean


  tableSawData = [
    { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
    { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
    { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
    { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
    { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
    { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
    { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
    { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' }
  ]


  TablesawConfig = {
    swipeHorizontalThreshold: 15,
    swipeVerticalThreshold: 20
  };

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  addNew() {
    const dialogRef = this.dialog.open(AddNewRowPopupComponent, {
      data: {},
      width: '700px',
      panelClass: 'lms-questions-popup'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  statistics() {
    const dialogRef = this.dialog.open(StatisticsPopupComponent, {
      data: {},
      width: '700px',
      // panelClass: 'add-case-mobile-popup'
      panelClass: 'lms-questions-popup'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }


  showpopUpSearch() {
    const dialogRef = this.dialog.open(SearchPopupComponent, {
      data: {},
      width: '245px',
      // panelClass: 'add-case-mobile-popup'
      panelClass: 'lms-questions-popup'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
  showSearch() {
    this.showDropdown = !this.showDropdown
  }
  showIndicatorDropdown() {
    this.indicatorDropdown = !this.indicatorDropdown
  }
}

export interface ICaseRowData {
  Indicator: string;
  RulesofIndicator: string;
  Sector: string;
  Unit: string;
  Frequency: string,
  Nov: string,
  Dec: string

}
const ELEMENT_DATA: ICaseRowData[] = [
  { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
  { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
  { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
  { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
  { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
  { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
  { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' },
  { Indicator: 'Total (fixed) Internet subscription', RulesofIndicator: 'Indicators required monthly', Sector: 'EBU/CBU', Unit: 'Subscription No', Frequency: 'Monthly', Nov: '3054870', Dec: '3054870' }
];


