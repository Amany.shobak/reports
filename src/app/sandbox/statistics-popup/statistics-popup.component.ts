import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-statistics-popup',
  templateUrl: './statistics-popup.component.html',
  styleUrls: ['./statistics-popup.component.scss']
})
export class StatisticsPopupComponent implements OnInit {

  constructor() { }
  Highcharts: typeof Highcharts = Highcharts;

  ngOnInit(): void {
  }


  StatisticsChart: any = {
    chart: {
      type: 'column',
      ignoreHiddenSeries: true,
    },
    title: {
      text: null
    },
    legend: {
      enabled: false
    },
    yAxis: {
      tickWidth: 1,
      tickColor: '#BBC8CE',
      startOnTick: true,
      endOnTick: true,
      lineWidth: 1,
      gridLineWidth: 0,
      lineColor: '#BBC8CE',
      title: {
        text: null
      },

      labels: {
        // step: 1,
        // format: '{value}%',
        overflow: 'justify',
        style: {
          color: '#1D252D',
          fontSize: '10px',
          fontWeight: 'light',
          fontFamily: 'BetaSTC'
        }
      }
    },
    xAxis: {
      categories: ['Sector1', 'Sector2', 'Sector3', 'Sector4', 'Sector5', 'Sector6', 'Sector7'],
      title: {
        text: null
      },

      tickColor: '#BBC8CE',
      startOnTick: true,
      endOnTick: true,
      lineWidth: 1,
      gridLineWidth: 0,
      lineColor: '#BBC8CE',
      labels: {
        step: 1,

        style: {
          color: '#1D252D',
          fontSize: '10px',
          fontWeight: '500',
          fontFamily: 'BetaSTC'
        }
      },
    },
    tooltip: {
      useHTML: true,
      headerFormat: '',
      pointFormat:
        `<div class="tooltip-chart">
            <span class="square mx-1" style="display:inline-block;width:9px; height:9px; background-color: {point.color}">
            </span><span class="mx-1">{point.y}/{point.stackTotal}</span></div>`
    },
    plotOptions: { column: { stacking: 'normal', pointPadding: 0.2, borderWidth: 0, pointWidth: 20 } },
    series: [
      { type: 'column', name: 'Done', color: '#FF375E', data: [15, 13, 5, 6, 7, 12, 25] },
      { type: 'column', name: 'In Progress', color: '#FF375E', data: [51, 31, 12, 13, 20, 23, 2] }
    ]
  };

}
