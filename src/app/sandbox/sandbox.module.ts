import { SharedModule } from './../shared/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SandboxRoutingModule } from './sandbox-routing.module';
import { ReportsComponent } from './reports/reports.component';
import { AddNewRowPopupComponent } from './add-new-row-popup/add-new-row-popup.component';
import { MaterialModule } from '../material.module';
import { StatisticsPopupComponent } from './statistics-popup/statistics-popup.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { SatPopoverModule } from '@ncstate/sat-popover';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchPopupComponent } from './search-popup/search-popup.component';


@NgModule({
  declarations: [ReportsComponent, AddNewRowPopupComponent, StatisticsPopupComponent, SearchPopupComponent],
  imports: [
    CommonModule,
    SandboxRoutingModule,
    MaterialModule,
    HighchartsChartModule,
    BrowserAnimationsModule,
    SatPopoverModule,
    SharedModule
  ]
})
export class SandboxModule { }
