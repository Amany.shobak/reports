import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'sandbox', loadChildren: () => import('./sandbox/sandbox-routing.module').then(m => m.SandboxRoutingModule)},
  {
    path: '',
    redirectTo: 'reports',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
